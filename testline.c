#include <stdio.h>
#include <string.h>
#include "src/list/src/list.h"

typedef enum InfoRet {
  IR_UNKNOWN = -1,
  IR_NORMAL = 0,
  IR_UNSUCCESS
} InfoRet;


typedef struct InfoState {
  char* info;
  list_node_t *pos1, *pos2, *pos3;
  list_t *wrap;
} InfoState;



void lcd_blank_line() {
  for (int i = 0; i < 15; ++i) {
    //lcd.print(' ');
    putchar(' ');
  }
}

int lcd_print_between(char* str, int pos1, int pos2) {
  //int i = 0;
  int va = 0;
  
  char* p = str+pos1;
  //int slen = strlen(str);
  while (p < str+pos2) {
    if (*p != '\n' && *p != '\r') {
      //lcd.print(*(p++));
      putchar(*p);
      ++va;
    } //else 
    //if (i == 0){ ++i; } else 
    //{
    //  break;
    //}
    ++p;
  }
  
  if (va < 15) {
    for (int j = 15 - va; j > 0; --j) {
      //lcd.print(' ');
      putchar(' ');
    }
  }
  putchar('!');
  return va;
}

list_t* aux_gen_wrap_list(list_t* wrap, char* text) {
  if (wrap == NULL) {
    wrap = list_new();
  }
  char* curr = text;
  int suf = 0;
  list_node_t *node;
  if (list_rpush(wrap, list_node_new(-1)) == NULL) {
    return NULL;
  }
  while (*curr != '\0') {
    //printf("suf: %d\tpos: %d\n", suf, (unsigned int)(curr-text));
    if (suf < 15) {
      if (*curr != '\n' && *curr != '\r') {
        ++suf;
      } else {
        if (list_rpush(wrap, list_node_new((unsigned int)(curr-text))) == NULL) {
          return NULL;
        }
        suf = 0;
      }
    } else {
      if (list_rpush(wrap, list_node_new((unsigned int)(curr-text))) == NULL) {
        return NULL;
      }
      suf = (*curr != '\n' && *curr != '\r') ? 1 : 0;      
    }
    ++curr;
  }
  if (list_rpush(wrap, list_node_new((int)(curr-text))) == NULL) {
    return NULL;
  }
  return wrap;
}

void initInfoState(InfoState* st, char* text) {
  st->info = text;
  st->wrap = list_new();
  st->wrap = aux_gen_wrap_list(st->wrap, st->info);
  st->pos1 = list_at(st->wrap, 0);
  st->pos2 = st->pos3 = NULL;
}

InfoRet initInfoScreen(InfoState* st, char* text) {
  initInfoState(st, text);
  //printf("%d\n", st->wrap->len);
  if (st->pos1 == NULL) {
    return IR_UNKNOWN;
  }
  //lcd.setCursor(0, 0);
  if ((st->pos2 = st->pos1->next)) {
    //printf("hello\n");
    //printf("%d\n", st->pos2->val);
    int tmp = lcd_print_between(st->info, 0, (int)st->pos2->val);
    //printf("%d\n", tmp);
    //printf("%d\n", st->wrap->len);
    if ((st->pos3 = st->pos2->next)) {
      printf("\n");
      //printf("%d\n", st->pos2->val);
      //printf("%d\n", st->pos3->val);
      int tmp = lcd_print_between(st->info, (int)st->pos2->val, (int)st->pos3->val);
      //printf("%d\n", tmp);
      //printf("%d\n", st->wrap->len);
    } else {
      printf("\n");
      lcd_blank_line();
    }
  } else {
    return IR_UNKNOWN;
  }
  return IR_NORMAL;
}

InfoRet scrollDown(InfoState* st) {
  if (st->pos2) {
    st->pos1 = st->pos2;
  } else {
    return IR_UNKNOWN;
  }
  if (st->pos3) {
    st->pos2 = st->pos3;
  } else {
    return IR_UNSUCCESS;
  }
  int tmp = lcd_print_between(st->info, (int)st->pos1->val, (int)st->pos2->val);
  if ((st->pos3 = st->pos3->next)) {
    printf("\n");
    int tmp = lcd_print_between(st->info, (int)st->pos2->val, (int)st->pos3->val);
  } else {
    printf("\n");
    lcd_blank_line();
  }
  return IR_NORMAL;
}

InfoRet scrollUp(InfoState* st) {
  if (st->pos2) {
    st->pos3 = st->pos2;
  } else {
    return IR_UNKNOWN;
  }
  
  if (st->pos1 == NULL) {
    return IR_UNKNOWN;
  } else if ((int)st->pos1->val == -1) {
    return IR_UNSUCCESS;
  } else {
    st->pos2 = st->pos1;
  }
  
  if ((st->pos1 = st->pos1->prev)) {
    int tmp = lcd_print_between(st->info, (int)st->pos1->val, (int)st->pos2->val);
  } else {
    return IR_UNKNOWN;
  }
  printf("\n");
  int tmp = lcd_print_between(st->info, (int)st->pos2->val, (int)st->pos3->val);
  return IR_NORMAL;
}

void destoryInfoState(InfoState* st) {
  st->pos1 = st->pos2 = st->pos3 = NULL;
  list_destroy(st->wrap);
  st->info = NULL;
}

int main() {
  char* cinfo = "Far far away, a\n\nbehind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.";
  //             0              1516               32
  char* s2 = "      ba\n      ab";
  //          0          11
  //list_t *wrap = list_new();
  //wrap = aux_gen_wrap_list(wrap, cinfo);
  
  //list_node_t *node;

  //node = list_at(wrap, 0);
  //printf("node val: %d\n", node->val);
  //printf("node prev: %p\n", node->prev);

  
  InfoRet ret_code;
  InfoState st;
  initInfoScreen(&st, cinfo);
  for (int i = 0; i < 10; ++i) {
    printf("\n");
    ret_code = scrollDown(&st);
    printf("\n%d\n", ret_code);
  }
  
  for (int i = 0; i < 10; ++i) {
    printf("\n");
    ret_code = scrollUp(&st);
    printf("\n%d\n", ret_code);
  }
  
 /* 
  while ((node = list_iterator_next(it))) {
    printf("%d\n", (unsigned int)(node->val));
  }
  */
  /*
  if ((node = list_iterator_next(it))) {
    
  }
    
  if ((node = list_iterator_next(it))) {
    //if (node->val == -1) {
    char* p = cinfo;
    list_node_t *test;
    while (*p != '\0') {
      //printf("node->val: %d\n", node->val);
      if (p - cinfo != node->val) {
        printf("%c", *p);
        //printf("%d %d\n", (unsigned int)(p - cinfo), (unsigned int)(node->val));
      } else {
        printf("|\n");
        if (*p != '\n' && *p != '\r') {
          printf("%c", *p);
        }
        if ((test = list_iterator_next(it))) {
          node = test;
        }
      }
      ++p;
    }
  } else {
    printf("%s", cinfo);
  }
  */
  /*
  char line[16];
  char* pos = aux_sub_line(cinfo, line);
  printf("%d\n%s\n", pos-cinfo, line);
  for (int i = 0; i < 4; ++i) {
    pos = aux_sub_line(pos, line);
    printf("%d\n%s\n", pos-cinfo, line);
  }
  for (int i = 0; i < 4; ++i) {
    pos = aux_sub_line_rev(pos-1, line);
    printf("%d\n%s\n", pos-cinfo, line);
  }
  */
  //list_iterator_destroy(it);
  //list_destroy(wrap);
  destoryInfoState(&st);
  return 0;
}